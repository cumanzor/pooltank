﻿ using System;
using System.Collections.Generic;
 using System.ComponentModel;
 using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.Data.Entity;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            //TODO: classes are public for testing purposes, make classes protected.
            //TODO: move this shit to unit tests
            //TODO: possible bug may cause duplicate reservation under bizarre conditions
            //TODO: Id property is ignored as it is automatically set primary key ident etc
            //TODO: Recheck Cascade conventions as we are doing some nasty shit in there.

            Test_EntityFramework_1();
            //Test_Linq_Unique_Ordering();
            //Test_Read_From_DB();
        }


        public static void Test_Read_From_DB()
        {
            using (var db = new EntityController())
            {
                var t = new DateTime(2014, 1, 1, 1, 1, 1);
                var resManager = new ReservationManager(db);
                var r = new Reservation(0, 1, 1, t);
                Console.WriteLine(resManager.AddNewReservation(r));
                //Console.WriteLine(r);
                //Console.ReadLine();
                Console.WriteLine(resManager.RemoveReservation(r));
            }
        }

        public static void Test_EntityFramework_1()
        {
            using (var db = new EntityController())
            {
                var condo = new Condo(0, "Test Condo 1", "Somewhere over there");
                db.Condos.Add(condo);
                db.SaveChanges();

                var apt = condo.AddApartment(
                    new Apartment(
                        0,condo.Id,"06",1,"John","blah@blah.com"));
                db.Apartments.Add(apt);
                db.SaveChanges();

                var area = condo.AddArea(
                    new Area(
                        0,condo.Id, "Pool", 10));
                
                db.Areas.Add(area);
                db.SaveChanges();

                //var res = new Reservation(0, apt.Id, area.Id, DateTime.Now);
                //db.Reservations.Add(res);
                //db.SaveChanges();

                //Console.ReadLine();

                //bring up list of existing stuff
                var apts = db.Apartments;
                var areas = db.Areas;
                Console.WriteLine(apts.ToString());
                var a = apts.ToList();
                Console.WriteLine(apts.ToString());
                Console.ReadLine();
            }
            
        }


        public static void Test_Linq_Unique_Ordering()
        {
            Condo c = new Condo(0, "Test Condos", "Here and there St.");

            //create 10 apartments
            var aptArray = new Apartment[10];
            for (var i = 0; i < 10; i++)
            {
                var count = i;
                var name = "John Tenant " + i;
                aptArray[i] = new Apartment(i,1, "" + i, 1, name,"email@email.com");
                c.AddApartment(aptArray[i]);
            }

            //create areas
            Area ar1 = new Area(0,1, "Pool", 15);
            Area ar2 = new Area(1,1, "BBQ", 20);
            Area ar3 = new Area(2,1, "Rooftop", 10);

            c.AddArea(ar1);
            c.AddArea(ar2);
            c.AddArea(ar3);

            //introduce 10 dates on 1 day difference
            var dates = new DateTime[10];
            for (var i = 1; i < 10; i++)
            {
                dates[i] = new DateTime(2014, 8, i, 14, 00, 00);
            }

            Shuffle(dates);

            Console.Write(c.ToString());
            Console.WriteLine(c.PrintAreas());
            Console.WriteLine(c.PrintApts());

            ReservationManager rm = new ReservationManager(null);

            for (var i = 1; i < 10; i++)
            {
                Console.WriteLine(rm.AddNewReservation(new Reservation(i, aptArray[i - 1].Id, ar1.Id, dates[i])));
                Console.WriteLine(rm.ReservationList.ElementAt(i - 1).ToString());

            }

            var ol = rm.GetOrderedReservations();

            //lazy attempt at adding repeated reservations
            Console.WriteLine("Shuffling...");
            Shuffle(dates);
            for (var i = 1; i < 10; i++)
            {
                Console.WriteLine(rm.AddNewReservation(new Reservation(i, aptArray[i - 1].Id, ar1.Id, dates[i])));
                Console.WriteLine(rm.ReservationList.ElementAt(i - 1).ToString());
            }



            Console.ReadKey();  
        }

        //generic Fischer-Yates shuffle.
        //it's all over the internet.
        public static void Shuffle<T>(T[] array)
        {
            var random = new Random();
            for (var i = array.Length; i > 1; i--)
            {
                int j = random.Next(i);
                T tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
        }

    }
}
