﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Domain
{
    public class Reservation : IComparable<Reservation>
    {
        public int id { get; set; }
        public DateTime datetime { get; set; }
        public long uniqueReservationId { get; set; }
        public Decimal UnixTimeStamp { get; set; }
        public int Guests { get; set; }

        #region navproperties
        public int ApartmentId { get; set; }
        public virtual Apartment Apartment { get; set; }

        public int AreaId { get; set; }
        public virtual Area Area { get; set; }
        #endregion

        public Reservation(int id, int aptId, int areaId, DateTime datetime) 
        {
            this.id = id;
            this.ApartmentId = aptId;
            this.AreaId = areaId;
            this.datetime = datetime;
            this.UnixTimeStamp = getUnixTimeStamp(datetime);
            this.uniqueReservationId = (getUnixTimeStamp(datetime) + new Random().Next(aptId,1000));
            
        }

        public Reservation()
        {
        }

        /// <summary>
        /// Gets the Unix Epoch time from the current datetime value.
        /// </summary>
        /// <param name="value">The DateTime object to convert.</param>
        /// <returns>value represented in milliseconds since the Unix Epoch</returns>
        private long getUnixTimeStamp(DateTime value)
        {
            //TODO: This might become an issue if the app itself is used in different
            //timezones.
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            return Convert.ToInt64(span.TotalSeconds);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Reservation: ");
            sb.AppendLine("Id: " + this.id);
            sb.AppendLine("Tenant: " + Apartment.Tenant);
            sb.AppendLine("Area: " + Area.Name);
            sb.AppendLine("Time: " + datetime);
            sb.AppendLine("Unique Id: " + uniqueReservationId);

            return sb.ToString();
        }

        public int CompareTo(Reservation other)
        {
            if (this.uniqueReservationId < other.uniqueReservationId) return -1;
            return this.uniqueReservationId > other.uniqueReservationId ? 1 : 0;
        }
    }
}
