﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Domain
{
    public class EntityController : DbContext
    {
        

        public EntityController()
        {
            //for debugging purpposes we are dropping the DB every time it runs.
            //Database.SetInitializer<EntityController>(new DropCreateDatabaseAlways<EntityController>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }


        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Condo> Condos { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

    }
}
