﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Domain
{
    public class Condo
    {

        #region EntityFramework Navigation Properties
        public virtual List<Apartment> ListOfApartments { get; set; }
        public virtual  List<Area> ListOfAreas { get; set; }
        #endregion  

        public int Id { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }

        public int NumOfApts
        {
            get
            {
                return ListOfApartments.Count;
            }
        }

        public int NumOfAreas
        {
            get
            {
                return ListOfAreas.Count;
            }
        }

        public int OccupationLimit { get; set; }
        public int FreeApts { get; set; }

        public Condo(int id, String name, String address)
        {
            ListOfApartments = new List<Apartment>();
            ListOfAreas = new List<Area>();

            this.Id = id;
            this.Name = name;
            this.Address = address;

        }

        public Condo()
        {
            
        }

        public Apartment AddApartment(Apartment a)
        {
            ListOfApartments.Add(a);
            return a;
        }

        public Apartment AddApartment(int id, String aptNumber, int floor, String tenant, String email)
        {
            var apt = new Apartment(id, this.Id, aptNumber, floor, tenant, email);
            ListOfApartments.Add(apt);
            return apt;
        }

        public Area AddArea(Area a)
        {
            ListOfAreas.Add(a);
            return a;
        }

        public Area AddArea(int id, string name, int max)
        {
            var area = new Area(id,this.Id, name, max);
            ListOfAreas.Add(area);
            return area;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Condo Information");
            sb.AppendLine("Name: " + this.Name);
            sb.AppendLine("Address: " + this.Address);
            sb.AppendLine("Number of Apartments: " + this.NumOfApts);
            sb.AppendLine("Number of Areas: " + this.NumOfAreas + Environment.NewLine);
            return sb.ToString();
        }

        public String PrintAreas()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var area in ListOfAreas)
            {
                sb.AppendLine(area.ToString());
            }
            return sb.ToString();
        }

        public String PrintApts()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var apt in ListOfApartments)
            {
                sb.AppendLine(apt.ToString());
            }
            return sb.ToString();
        }



        //private void fillApartments(List<Apartment> l)
        //{

        //}

        //private void fillAreas(List<Area> l)
        //{

        //}





    }
}
