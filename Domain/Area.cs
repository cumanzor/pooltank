﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Area
    {
        #region fields
        public int Id { get; set; }
        
        public String Name { get; set; }
        
        public int MaxOccupancy { get; set; }
        #endregion

        #region nav properties

        public int CondoId { get; set; }
        public virtual Condo Condo { get; set; }
        #endregion

        #region Constructors
        public Area(int id, int condoId, String name, int maxOccupancy)
        {
            this.Id = id;
            this.Name = name;
            this.MaxOccupancy = maxOccupancy;
            this.CondoId = condoId;
        }

        public Area()
        {
            
        }

        #region toStrings
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Area Info");
            sb.AppendLine("Name: " + this.Name);
            sb.AppendLine("Max Occupancy: " + this.MaxOccupancy);
            return sb.ToString();
        }
        #endregion
    }
        #endregion 

}