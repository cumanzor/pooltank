﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Apartment
    {
        #region fields
        //all this is equivalent to an autoproperty in C#
        //where the compiler automatically generates the private field.
        //the only reason to use this convention is mostly for input validation
        //or special output.
        private String _aptNumber;
        
        public string Email { get; set; }

        public String PasswordEncrypted { get; set; }
        
        public string Tenant { get; set; }
        //[Key] by convention, CodeFirst will use any field called ID as the primary key
        public int Id { get; set; }

        public int Floor { get; set; }

        
        public String AptNumber
        {
            set
            {
                _aptNumber = value;
            }
            get
            {
                return Floor + _aptNumber;
            }
        }

        

        #endregion

        #region nav properties

        public int CondoId { get; set; }
        public virtual Condo Condo { get; set; }
        #endregion

        #region constructors
        public Apartment(int id, int condoId, String aptNumber, int floor, String tenant, String email)
        {
            this.CondoId = condoId;
            this.Id = id;
            this.AptNumber = aptNumber;
            this.Floor = floor;
            this.Tenant = tenant;
            this.Email = email;
        }

        public Apartment()
        {
            
        }
        #endregion  

        #region toStrings
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Apartment Info ");
            sb.AppendLine("Apartment Number: " + this.AptNumber);
            sb.AppendLine("Apartment Floor: " + this.Floor);
            sb.AppendLine("Apartment Tenant: " + this.Tenant);
            return sb.ToString();
        }
        #endregion

    }
}
