﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ReservationManager
    {
        public List<Reservation> ReservationList{ get; set; }
        public List<Area> AreaList { get; set; }
        public List<Apartment> ApartmentList { get; set; }

        public EntityController ec;

        public ReservationManager(EntityController ec)
        {
            ReservationList = new List<Reservation>();
            AreaList = new List<Area>();
            ApartmentList = new List<Apartment>();
            this.ec = ec;
            FillApartments();
            FillAreas();
            FillReservations();
        }

        public void FillReservations()
        {
            ReservationList = ec.Reservations.ToList();
        }

        public void FillApartments()
        {
            ApartmentList = ec.Apartments.ToList();
        }

        public void FillAreas()
        {
            AreaList = ec.Areas.ToList();
        }

        public IEnumerable<Reservation> GetOrderedReservations()
        {

            IEnumerable<Reservation> results = from res in ReservationList
                orderby res
                select res;
            return results;
        }
        
       
        public bool AddNewReservation(Reservation r)
        {
            //check if reservations already exist
            if (ReservationList.Any(res => res.AreaId == r.AreaId  && 
                                           res.ApartmentId == r.ApartmentId &&
                                           res.UnixTimeStamp == r.UnixTimeStamp)) return false;
            ec.Reservations.Add(r);
            ec.SaveChanges();
            Resync();
            return true;
        }


        
        public bool RemoveReservation(Reservation r)
        {
            if (!ReservationList.Any(res => res.AreaId == r.AreaId &&
                                            res.ApartmentId == r.ApartmentId &&
                                            res.UnixTimeStamp == r.UnixTimeStamp)) return false;
            ec.Reservations.Remove(r);
            ec.SaveChanges();
            Resync();
            return true;
        }

        public bool EditReservation(Reservation r)
        {
            throw new NotImplementedException();
        }

        private void Resync()
        {
            FillApartments();
            FillAreas();
            FillReservations();
        }
        

    }
}
